package OOP;
import java.lang.*;
import java.awt.Color;

public class Horse{

	public String nama;
	public Color warna;
	public int usia;
	public boolean statusJinak;
	public double beratBadan;
	public String majikan;

	public void cetakInformasi(){
	
		System.out.println("Kuda bernama: " +nama);
		System.out.println("Warna kuda: " +warna);
		System.out.println("Usia : " +usia);
		System.out.println("Jinak ?: " +statusJinak);
		System.out.println("Diadopsi oleh: " +majikan);
	}

	public void diadopsi(String m){
	
		majikan = m;
		statusJinak = true;
	}

	public boolean apakahJinak(){

		return(statusJinak);
	}

}


	