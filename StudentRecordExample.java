public class StudentRecordExample{
	
	public static void main(String [] args){
	
	//Membuat 3 object student

		StudentRecord annaRecord = new StudentRecord();
		StudentRecord beahRecord = new StudentRecord();
		StudentRecord crisRecord = new StudentRecord();

	//Versi baru yang ditambahkan

		StudentRecord karyono = new StudentRecord("Karyono");
		StudentRecord songjongki = new StudentRecord("Song Jong - Ki","Cibaduyut");
		StudentRecord masbejo = new StudentRecord(80,90,100);

	//Memberi nama siswa

		annaRecord.setName("Anna");
		beahRecord.setName("Beah");
		crisRecord.setName("Cris");

	//Menampilkan nama siswa

		System.out.println("Nama: "+annaRecord.getName());
		System.out.println("Nama: "+beahRecord.getName());
		System.out.println("Nama: "+crisRecord.getName());

	//Menampilkan jumlah siswa
	
		System.out.println("Count: "+StudentRecord.getStudentCount());
		StudentRecord anna2Record = new StudentRecord();
		anna2Record.setName("Anna");
		anna2Record.setAddress("Philipines");
		anna2Record.setAge(15);
		anna2Record.setMathGrade(80);
		anna2Record.setSciGrade(95.5);
		anna2Record.setEngGrade(100);
	
	//Overload method

		anna2Record.print(anna2Record.getName());
		anna2Record.print(anna2Record.getEngGrade(),anna2Record.getMathGrade(),anna2Record.getSciGrade());
		anna2Record.getMathGrade();
		anna2Record.getSciGrade();
	}
}