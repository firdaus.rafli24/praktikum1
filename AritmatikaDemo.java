public class AritmatikaDemo
{
	public static void main(String[] args)
	{
		//Some numbers
		
			int i = 37;
			int j = 42;
			double x = 27.475;
			double y = 7.22;
	
		//Output console
	
			System.out.println("Variable Values...");
			System.out.println("i = " + i);
			System.out.println("j = " + j);
			System.out.println("x = " + x);
			System.out.println("y = " + y);
			
		//Adding Numbers
	
			System.out.println("Adding...");
			System.out.println("i + j = " + (i + j));
			System.out.println("x + y = " + (x + y));
			
		//Substraction
	

			System.out.println("Substraction...");
			System.out.println("i - j = " + (i - j));
			System.out.println("x - y = " + (x - y));
			
		//Number Multiplier
	
			System.out.println("Multiplying...");
			System.out.println("i * j = " + (i * j));
			System.out.println("x * y = " + (x * y));
			
		//Divide number
	
			System.out.println("Dividing...");
			System.out.println("i / j = " + (i / j));
			System.out.println("x / y = " + (x / y));

		//Counting Modulus Result
	
			System.out.println("Computing the remainder...");
			System.out.println("i % j = " + (i % j));
			System.out.println("x % y = " + (x % y));

		
		//Mixing Type

			System.out.println("Mixing Types...");
			System.out.println("j + y = " + (j + y));
			System.out.println("i + x = " + (i + x));
	}
}