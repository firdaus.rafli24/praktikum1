import  java.lang.*;

public class PrivateElevator{

	private boolean bukaPintu = false;
	private int lantaiSkrng = 1;
	private int berat = 0;

	private final int KAPASITAS = 1000;
	private final int LANTAI_ATAS = 5;
	private final int LANTAI_BAWAH = 1;

	private void hitungKapasitas(){
		
		berat = (int)(Math.random()*1500);
		System.out.println("Berat: "+berat);
	}

	public void buka(){
		
		bukaPintu = true;
	}

	public void tutup(){

		hitungKapasitas();
		if(berat <= KAPASITAS){

			bukaPintu = false;
		}else{

			System.out.println("Elevator kelebihan beban!!");
			System.out.println("Pintu akan terbuka sampai satu orang keluar");
		}
	}

	public void naik(){

		if(!bukaPintu){
		
			if(lantaiSkrng < LANTAI_ATAS){

				lantaiSkrng++;
				System.out.println(lantaiSkrng);
			}else{

				System.out.println("Sudah mencapai lantai atas");
			}
		}else{

			System.out.println("Pintu masih terbuka");
		}
	}

	public void turun(){

		if(!bukaPintu){
		
			if(lantaiSkrng > LANTAI_BAWAH){

				lantaiSkrng--;
				System.out.println(lantaiSkrng);
			}else{
			
				System.out.println("Sudah mencapai lantai paling bawah");
			}
		}else{

			System.out.println("Pintu masih terbuka");
		}
	}

	public void setLantai(int tujuan){
	
		if(tujuan > LANTAI_BAWAH && tujuan < LANTAI_ATAS){

			while(lantaiSkrng != tujuan){
		
				if(lantaiSkrng > tujuan){
			
					turun();
				}else{

					naik();
				}
			}
		}else{
			
			System.out.println("Lantai Salah");
		}
	}

	public int getLantai(){

		return lantaiSkrng;
	}

	public boolean getStatusPintu(){

		return bukaPintu;
	}
}