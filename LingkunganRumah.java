import OOP.Kucing;
import java.awt.Color;
import java.lang.*;

public class LingkunganRumah
{
	
	public static void main(String [] args)
	{
		
		Kucing michael = new Kucing();
		michael.warnaBulu = new Color (0,1,1);
		michael.nama = "Michael";
		michael.usia = 3;
		michael.beratBadan = 4.5;
		michael.diadopsi("Rezki");
		Kucing garfield = new Kucing();
		garfield.warnaBulu = new Color (0,2,2);
		garfield.nama = "Garfield";
		garfield.usia = 1;
		garfield.beratBadan = 2.3;
		garfield.diadopsi("Rezki");
		//michael.cetakInformasi();
		System.out.print("\n");
		//garfield.cetakInformasi();
	}
}