public abstract class LivingThings{

	public void breath(){

		System.out.println("Living Thing Breathing...");

	}

	public void eat(){

		System.out.println("Living Thing eating...");
	}

	public abstract void walk();
}