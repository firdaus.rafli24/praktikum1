public class ElevatorTestTwo{

	public static void main(String [] args){
	
		Elevator myElevator = new Elevator();
		
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.goUp();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		myElevator.openDoor();
		myElevator.closeDoor();
		myElevator.goDown();
		int currFloor = myElevator.getFloor();
		System.out.println("Current Floor: "+currFloor);
		myElevator.setFloor(currFloor + 1);
		myElevator.openDoor();
	}
}
		