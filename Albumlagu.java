class AlbumLagu
{
	
	String judul;
	String artis;
	int thnRilis;
	
	//Konstruktor
	public AlbumLagu(String judul,String artis,int thnRilis)
	{
	
		this.judul = judul;
		this.artis = artis;
		this.thnRilis = thnRilis;
	}

	public String infoJudul()
	{
	
		return(judul);
	}

	public String infoArtis()
	{
	
		return(artis);
	}

	public int infothnRilis()
	{
	
		return(thnRilis);
	}

	public static void main(String [] args)
	{
	
		AlbumLagu album = new AlbumLagu("I Love Java Code","Rfl",2018);
		System.out.println("Judul Lagu = "+ album.infoJudul());
		System.out.println("Artis = "+ album.infoArtis());
		System.out.println("Tahun Rilis = "+ album.infothnRilis());
	}
}
	