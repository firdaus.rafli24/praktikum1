import OOP.Lion;
import OOP.Horse;
import OOP.Kangaroo;
import java.lang.*;
import java.awt.Color;

public class Zoo{

	public static void main(String [] args){
	
		Lion simba = new Lion();
		simba.nama = "Simba";
		simba.usia = 10;
		simba.beratBadan = 190;
		simba.diadopsi("Steve Irwin");
		simba.cetakInformasi();
		System.out.print("\n");

		Horse roach = new Horse();
		roach.nama = "Roach";
		roach.warna = new Color(165,42,42);
		roach.usia = 20;
		roach.beratBadan = 199;
		roach.diadopsi("Geralt");
		roach.cetakInformasi();
		System.out.print("\n");

		Kangaroo jack = new Kangaroo();
		jack.nama = "Jack";
		jack.warna = new Color(165,42,42);
		jack.usia = 5;
		jack.beratBadan = 50;
		jack.diadopsi("Rene");
		jack.cetakInformasi();
		
			
	}
}
		