public class Pakaian{
	
	private int ID = 0;
	private String keterangan ="-keterangan diperlukan-";
	private double harga = 0.0;
	private int jmlStock = 0;

	private static int UNIQUE_ID = 0;

	public Pakaian(){
			
		ID = UNIQUE_ID++;

	}

	public int getID(){

		return ID;
	}

	public void setKeterangan(String keterangan){
	
		this.keterangan = keterangan;
	}

	public String getKeterangan(){

		return keterangan;
	}

	public void setHarga(double harga){
	
		this.harga = harga;
	}

	public double getHarga(){

		return harga;
	}

	public void setJmlStock(int jmlStock){
	
		this.jmlStock = jmlStock;
	}

	public int getJmlStock(){
		
		return jmlStock;
	}
}
	